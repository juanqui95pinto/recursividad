﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Dado  un numero entero N, No negativo, implementar un metodo
 * recursivo que permita saber cual es el factorial de dicho 
 * numero.
 * 
 * Caso Base:
 * - Cuando el numero es cero o uno, la respuesta directa es 1
 * 
 * Caso recursivo:
 * - En numero actual multiplicado por el factorial del numero
 * anterior f(n) = n*f(n-1)
 */
namespace Recursion.Exercise01.CountDigits
{
    internal class E02_Factorial
    {
        public int CalcularFactorial(int numero)
        {
            int result;
            if (numero == 0 || numero == 1)
            {
                result = 1;
            }else
            {
                result = numero * CalcularFactorial( numero-1);
            }
            return result;
        }

        public int CalcularFactorial2(int numero)
        {
            if (numero == 0 || numero == 1)
            {
                return 1;
            }
            else
            {
                return numero * CalcularFactorial2(numero - 1);
            }
        }
    }
}
