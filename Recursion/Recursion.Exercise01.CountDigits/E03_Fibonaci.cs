﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* Implementar un metodo recursivo que permita saber cual es el 
 * N-simo termino de la serie Fibonacci.
 * Caso Base:
 * f0 = 0
 * f1 = 1
 * Caso Recursivo:
 * fn = fn-1 + fn-2
 */
namespace Recursion.Exercise01.CountDigits
{
    internal class E03_Fibonaci
    {
        public int CalcFibo(int n)
        {
            int result;
            if (n == 0)
            {
                result = 0;
            }else if (n == 1)
                  {
                      result = 1;
                  }
                  else
                  {
                      result = CalcFibo(n-1) + CalcFibo(n-2);
                  }
            return result;
        }

        public int CalcFibo2(int n)
        {
            if (n == 0)
            {
                return 0;
            }
            else if (n == 1)
            {
                return 1;
            }
            else
            {
                return CalcFibo2(n - 1) + CalcFibo2(n - 2);
            }
        }
    }
}
