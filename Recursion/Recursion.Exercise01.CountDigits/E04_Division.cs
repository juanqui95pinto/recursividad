﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Dado dos numeros enteros positivos, implementar un metodo
 * recursivo que permita realizar la operacion aritmetica de 
 * division mediante restas sucesivas.
 * 
 * Caso Base:
 * cuando el dividendo sea menor que el divisor, el resultado
 * es cero. (para conteo)
 * 
 * Caso Recursivo:
 * - Restar dividiendo - divisor, el resultado sera el nuevo valor
 * de dividendo.
 * - Sumar + 1 al contador, por cada vez que puedas restar.
 * 
 * Casos excepcionales: -1 se entidende como: esta division no 
 * se puede hacer.
 */
namespace Recursion.Exercise01.CountDigits
{
    internal class E04_Division
    {
        public int Division(int dividendo, int divisor)
        {
            int result;
            if (divisor <= 0) 
            {
                result = -1;
            }
            else
            {
                if (dividendo < divisor)
                {
                    result = 0;
                }
                else
                {
                    result = 1 + Division(dividendo - divisor, divisor);
                }
            }
            
            return result;
        }

        public int Division2(int dividendo, int divisor)
        {
            if (divisor <= 0)
            {
                return -1;
            }
            else
            {
                if (dividendo < divisor)
                {
                    return 0;
                }
                else
                {
                    return 1 + Division2(dividendo - divisor, divisor);
                }
            }
        }
    }
}
