﻿namespace Recursion.Exercise01.CountDigits
{ 
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");

            //E01_ContarDigitos e01cd = new E01_ContarDigitos();
            //Console.WriteLine(e01cd.ContarDigitos(012345));
            //Console.WriteLine(e01cd.ContarDigitos2(0000012345));

            //E02_Factorial e02_Factorial = new E02_Factorial();
            //Console.WriteLine(e02_Factorial.CalcularFactorial(19));
            //Console.WriteLine(e02_Factorial.CalcularFactorial2(3));

            //E03_Fibonaci e03_Fibonaci = new E03_Fibonaci();
            //Console.WriteLine(e03_Fibonaci.CalcFibo(17));
            //Console.WriteLine(e03_Fibonaci.CalcFibo2(17));

            //E04_Division e04_Division = new E04_Division();
            //Console.WriteLine(e04_Division.Division(3, 0));
            //Console.WriteLine(e04_Division.Division2(23, 3));

            //E05_Multiplicacion e05_Multiplicacion = new E05_Multiplicacion();
            //Console.WriteLine(e05_Multiplicacion.MultiplicacionPorSumas(-4, 3));
            //Console.WriteLine(e05_Multiplicacion.MultiplicacionPorSumas2(-4, 3));

            //recursividad indirecta, la espuesta esta en el parametro
            E06_InvertirNumero e06 = new E06_InvertirNumero();
            Console.WriteLine(  e06.InvertirNumero(56789));

        }
    }
}