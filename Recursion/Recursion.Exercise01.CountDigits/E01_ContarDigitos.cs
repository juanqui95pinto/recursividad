﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Dado un numero natural N, se pide implementar un metodo
 * recursivo que permita saber la cantidad de digitos que
 * tiene dicho numero.
 * 
 * Caso Base: 
 * Cuando el numero sea menor q 10, 
 * la respuesta es 1 digito.
 * 
 * Caso Recusivo: 
 * - Dividir el numero entre 10
 * - Contar +1dig
 * - Llamada recursiva, con el nuevo valor.
 */
namespace Recursion.Exercise01.CountDigits
{
    internal class E01_ContarDigitos
    {
        public int ContarDigitos(int numero)
        {
            int res;
            if (numero < 10) 
            {
                res = 1;
            }
            else
            {
                numero = numero/10;
                res = 1 + ContarDigitos(numero);
            }
            return res;
        }

        public int ContarDigitos2(int numero)
        {
            if (numero < 10)
            {
                return 1;
            }
            else
            {
                numero = numero / 10;
                return 1 + ContarDigitos2(numero);
            }
        }
    }
}
