﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* Dado dos numeros enteros positivos, implementar un metodo
 * recursivo que permita realizar la operacion aritmetica de
 * multiplicacion mediante sumas sucesivas.
 * 
 * caso base:
 * - Cuando el multiplicador sea cero, la respuesta
 *   sera cero.
 * - Cuando el multiplicador llegue a uno, el resultado sera
 *   el valor del multiplicando.
 * 
 * caso recursivo:
 * - Sumar el valor del multiplicando a mi respuesta.
 * - Decrementar en 1, el valor de multiplicador.
 */
namespace Recursion.Exercise01.CountDigits
{
    internal class E05_Multiplicacion
    {
        public int MultiplicacionPorSumas(int multiplicando, int multiplicador)
        {
            int result;
            if (multiplicador <= 0)
            {
                result = 0;
            }
            else
            {
                if (multiplicador == 1)
                {
                    result = multiplicando;
                }
                else
                {
                    result = multiplicando + MultiplicacionPorSumas(multiplicando, multiplicador-1);
                }
            }

            return result;
        }

        public int MultiplicacionPorSumas2(int multiplicando, int multiplicador)
        {
            if (multiplicador <= 0)
            {
                return 0;
            }
            else
            {
                if (multiplicador == 1)
                {
                    return multiplicando;
                }
                else
                {
                    return multiplicando + MultiplicacionPorSumas2(multiplicando, multiplicador - 1);
                }
            }
        }
    }
}
