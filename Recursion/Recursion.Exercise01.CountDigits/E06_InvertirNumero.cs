﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* Dado un numero entero, no negativo, se  pide implementar un metodo
 * recursivo que te permita invertir (el orden de los digitos) de 
 * dicho numero.
 * (se realiza una serie de divisiones sucesivas entre 10 para obtener
 * los numeros volcados en los reciduos)
 * 
 * Caso base:
 * - Cuando el numero sea cero, retornas el valor de la variable result.
 * 
 * Caso recursivo:
 * - Calcular el residuo (n%10)
 * - Actualizar el numero, dividiendolo entre 10
 * - Actualizar el valor de result, aplicando la operacion aritmetica
 *   0*10 = 0+4 = 4
 *   
 *   RECURSIVIDAD INDIRECTA
 */
namespace Recursion.Exercise01.CountDigits
{
    internal class E06_InvertirNumero
    {
        public int InvertirNumero(int numero)
        { 
            int result = 0;
            return InvertirNumero(numero, result);
        }
        private int InvertirNumero(int numero, int result)
        {
            if (numero == 0)
            {
                return result;
            }
            else {
                int residuo = numero % 10;
                numero = numero / 10;
                result = (result * 10) + residuo;
                return InvertirNumero(numero, result);
            }
        }
    }
}
